import {Component, Input, OnInit, Output} from '@angular/core';


@Component({
  selector: 'app-modal-index',
  templateUrl: './modal-index.component.html',
  styleUrls: ['./modal-index.component.css']
})
export class ModalIndexComponent implements OnInit {
  display: boolean = false;

    showDialog() {
        this.display = true;
    }
  cita: Date;
  constructor() { }

  ngOnInit() {
  }

} 
