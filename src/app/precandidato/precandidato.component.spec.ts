import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrecandidatoComponent } from './precandidato.component';

describe('PrecandidatoComponent', () => {
  let component: PrecandidatoComponent;
  let fixture: ComponentFixture<PrecandidatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrecandidatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrecandidatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
