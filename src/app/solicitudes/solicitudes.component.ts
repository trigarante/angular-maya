import { Component, OnInit } from '@angular/core';
import {SolicitudService} from '../solicitud.service';
import {Solicitud} from '../modelos/solicitud.model'; 


// import {BsModalService, BsModalRef} from 'ngx-bootstrap/modal';
import {filter, map} from 'rxjs/operators';
// import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

// import {ModalIndexComponent} from '../modal-index/modal-index.component';

@Component({
  selector: 'app-solicitudes',
  templateUrl: './solicitudes.component.html',
  styleUrls: ['./solicitudes.component.css'],
  providers:[SolicitudService]

})
export class  SolicitudesComponent implements OnInit {
  solicitudes: Solicitud[];
  cols: any[];
  cita: Date;

  constructor(private _solicitudService: SolicitudService) {
    
   }
  
  getSolicitudes(){
    this._solicitudService.getSolicitudes().subscribe(	response => {
      if(response){
        this.solicitudes = response;
      }
    },
    error => {
      console.log(<any>error);
    }
    )
  }
  ngOnInit() {
    this.getSolicitudes();
    this.cols = [
      { field: 'id', header: 'id' },
            { field: 'nombre', header: 'nombre' },
            { field: 'telefono', header: 'telefono' },
            { field: 'correo', header: 'correo' },
            { field: 'cita', header: 'cita' },
            { field: 'acción', header: 'acción' },
            
       
    ];
    
  }

}
