import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPrecandidatoComponent } from './add-precandidato.component';

describe('AddPrecandidatoComponent', () => {
  let component: AddPrecandidatoComponent;
  let fixture: ComponentFixture<AddPrecandidatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPrecandidatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPrecandidatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
