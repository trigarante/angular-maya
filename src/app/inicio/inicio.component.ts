import { Component, OnInit } from '@angular/core';
import {EquipoService} from './../equipo.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  equipo:any[] =[];

  constructor(private _servicio:EquipoService) { 
    this.equipo = _servicio.getEquipo();
  }

  ngOnInit() {
  }

}
