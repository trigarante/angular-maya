
//Dependencias
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {PaginatorModule} from 'primeng/paginator';
import { AppRoutingModule } from './app-routing.module';
import {AccordionModule} from 'primeng/accordion';     //accordion and accordion tab
import {MenuItem} from 'primeng/api';                 //api
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { PanelModule } from 'primeng/components/panel/panel';
import { ButtonModule } from 'primeng/components/button/button';
import { RadioButtonModule } from 'primeng/components/radioButton/radioButton';
import {PanelMenuModule} from 'primeng/panelmenu';
import {DropdownModule} from 'primeng/dropdown';
import {TabMenuModule} from 'primeng/tabmenu';
import {TableModule} from 'primeng/table';
import {Component} from '@angular/core';
import {MessageService} from 'primeng/api';
import {DataViewModule} from 'primeng/dataview';
import {CardModule} from 'primeng/card';
import {DialogModule} from 'primeng/dialog';
import {MenuModule} from 'primeng/menu';
import {CalendarModule} from 'primeng/calendar';
import {ModalModule, BsModalService} from 'ngx-bootstrap/modal';
import {InputTextModule} from 'primeng/inputtext';



//servicios
import{EquipoService} from './equipo.service';
import{SolicitudService} from './solicitud.service';

//Componentes

import { AppComponent } from './app.component';
import { PrecandidatoComponent } from './precandidato/precandidato.component';
import { EncabezadoComponent } from './encabezado/encabezado.component';
import { FooterComponent } from './footer/footer.component';
import { ContactoComponent } from './contacto/contacto.component';
import { LayoutComponent } from './layout/layout.component';
import { InicioComponent } from './inicio/inicio.component';
import { from } from 'rxjs';
import { EquipoComponent } from './equipo/equipo.component';
import { SolicitudesComponent } from './solicitudes/solicitudes.component';
import { ModalIndexComponent } from './modal-index/modal-index.component';
import { AddPrecandidatoComponent } from './add-precandidato/add-precandidato.component';


//Ruteo de pagina
const routes: Routes = [
  { path: 'contacto', component: ContactoComponent },
  { path: 'equipo/:id', component: EquipoComponent },
  { path: 'inicio', component: InicioComponent },
  { path: 'precandidato', component: PrecandidatoComponent},
  { path: 'solicitud', component: SolicitudesComponent },
  { path: 'createPrecan', component: AddPrecandidatoComponent},
  { path: '',component: LayoutComponent, pathMatch: 'full' },
  { path: '**', redirectTo: '/', pathMatch: 'full' },
];

@NgModule({
  declarations: [
    AppComponent,
    PrecandidatoComponent,
    EncabezadoComponent,
    FooterComponent,
    ContactoComponent,
    LayoutComponent,
    InicioComponent,
    EquipoComponent,
    SolicitudesComponent,
    ModalIndexComponent,
    AddPrecandidatoComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes) ,
    FormsModule,
    HttpClientModule,
    PaginatorModule,
    AccordionModule,
    BrowserAnimationsModule,
    PanelModule,
    RadioButtonModule,
    AppRoutingModule,
    DropdownModule,
    TabMenuModule,
    TableModule,
    ButtonModule,
    PaginatorModule,
    DataViewModule,
    CardModule,
    DialogModule,
    MenuModule,
    PanelMenuModule,
    ReactiveFormsModule,
    CalendarModule,
    ModalModule,
    InputTextModule,

    
  ],
  providers: [SolicitudService, EquipoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
